GCC=mpic++

all: page_rank.cpp 
	$(GCC) ./page_rank.cpp -O3 -march=native -o main

run: all
# % prog GraphFile Partitionfile rounds Partitions 

# My own two test files
#	mpiexec -np 2 ./main test_undir.tab.txt test_domains.2.txt 5 2
#
#undir file
	mpiexec -np 2 ./main fl_undir.tab.txt fl_domains.2.txt 5 2

#Normal run files
#	mpiexec -np 2 ./main fl_compact.tab.txt fl_compact_part.2 5 2
#	mpiexec --oversubscribe -np 4 ./main fl_compact.tab.txt fl_compact_part.4 5 4

